﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Drivetrain))]
public class CarAI : MonoBehaviour
{
    public Wheel[] wheels;

    public Transform centerOfMass;
    public float inertiaFactor = 1.5f;

    float brake;
    float throttle;
    float throttleInput;
    float steering;
    float lastShiftTime = -1;
    float handbrake;

    Drivetrain drivetrain;

    public float shiftSpeed = 0.8f;


    public float throttleTime = 1.0f;
    public float throttleTimeTraction = 10.0f;
    public float throttleReleaseTime = 0.5f;
    public float throttleReleaseTimeTraction = 0.1f;

    public bool tractionControl = true;


    public float steerTime = 1.2f;
    public float veloSteerTime = 0.1f;

    public float steerReleaseTime = 0.6f;
    public float veloSteerReleaseTime = 0f;
    public float steerCorrectionFactor = 4.0f;

    private Rigidbody rb;

    public float slipVelo
    {
        get
        {
            float val = 0.0f;
            foreach (Wheel w in wheels)
                val += w.slipVelo / wheels.Length;
            return val;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (centerOfMass != null)
            rb.centerOfMass = centerOfMass.localPosition;
        rb.inertiaTensor *= inertiaFactor;
        drivetrain = GetComponent(typeof(Drivetrain)) as Drivetrain;
    }

    void Update()
    {
        Vector3 carDir = transform.forward;
        float fVelo = rb.velocity.magnitude;
        Vector3 veloDir = rb.velocity * (1 / fVelo);
        float angle = -Mathf.Asin(Mathf.Clamp(Vector3.Cross(veloDir, carDir).y, -1, 1));
        float optimalSteering = angle / (wheels[0].maxSteeringAngle * Mathf.Deg2Rad);
        if (fVelo < 1)
            optimalSteering = 0;

        bool accelKey = true; //Input.GetKey (KeyCode.UpArrow);
        bool brakeKey = Input.GetKey(KeyCode.DownArrow);

        if (drivetrain.automatic && drivetrain.gear == 0)
        {
            accelKey = Input.GetKey(KeyCode.DownArrow);
            brakeKey = Input.GetKey(KeyCode.UpArrow);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            throttle += Time.deltaTime / throttleTime;
            throttleInput += Time.deltaTime / throttleTime;
        }
        else if (accelKey)
        {
            if (drivetrain.slipRatio < 0.10f)
                throttle += Time.deltaTime / throttleTime;
            else if (!tractionControl)
                throttle += Time.deltaTime / throttleTimeTraction;
            else
                throttle -= Time.deltaTime / throttleReleaseTime;

            if (throttleInput < 0)
                throttleInput = 0;
            throttleInput += Time.deltaTime / throttleTime;
            brake = 0;
        }
        else
        {
            if (drivetrain.slipRatio < 0.2f)
                throttle -= Time.deltaTime / throttleReleaseTime;
            else
                throttle -= Time.deltaTime / throttleReleaseTimeTraction;
        }
        throttle = Mathf.Clamp01(throttle);

        if (brakeKey)
        {
            if (drivetrain.slipRatio < 0.2f)
                brake += Time.deltaTime / throttleTime;
            else
                brake += Time.deltaTime / throttleTimeTraction;
            throttle = 0;
            throttleInput -= Time.deltaTime / throttleTime;
        }
        else
        {
            if (drivetrain.slipRatio < 0.2f)
                brake -= Time.deltaTime / throttleReleaseTime;
            else
                brake -= Time.deltaTime / throttleReleaseTimeTraction;
        }
        brake = Mathf.Clamp01(brake);
        throttleInput = Mathf.Clamp(throttleInput, -1, 1);

        handbrake = Mathf.Clamp01(handbrake + (Input.GetKey(KeyCode.Space) ? Time.deltaTime : -Time.deltaTime));

        float shiftThrottleFactor = Mathf.Clamp01((Time.time - lastShiftTime) / shiftSpeed);
        drivetrain.throttle = throttle * shiftThrottleFactor;
        drivetrain.throttleInput = throttleInput;

        foreach (Wheel w in wheels)
        {
            w.brake = brake;
            w.handbrake = handbrake;
            w.steering = steering;
        }

    }
}
