Nordic Deer

Thank you for purchasing our product. We hope that the quality of our product is on par for your project
as we strive to keep the highest quality possible.

Regards Adventure Forge AS

Tris count: 3934

Vertice count: 2987

Textures Unity 4:
	Colormap with specularity 2048 X 2048 pxl
	Normalmap 2048 X 2048 pxl

Textures Unity 5:
	Colormap 2048 X 2048 pxl
	Specularmap 2048 x 2048 pxl
	Occlusionmap 2048 X 2048 pxl
	Normalmap 2048 X 2048 pxl
	Emissionmap 2048 X 2048 pxl

Setup for deer:

To make the animations as smooth as possible:
	Go in to Edit -> Project Settings -> Quality 
	and set Blend Weights to 4 bones.

	Also go in to Animations in the FBX's Inspector and set
	Anim. Compression to Optimal.


List of animations:
	
	1. 	Walk		- Frame 5 to 67
	2. 	Run		- Frame 70 to 92.5
	3. 	Idle		- Frame 120 to 350
	4.	Idle2		- Frame 351 to 712
	5.	Idle3		- Frame 713 to 970
	6.	Idle4		- Frame 971 to 1360
	7.	Idle5		- Frame 1361 to 1599
	8.	Hit1		- Frame 1600 to 1629
	9.	Hit2		- Frame 1630 to 1663
	10.	Hit3		- Frame 1664 to 1694
	11.	EnterCombat	- Frame 1695 to 1725
	12.	CombatIdle	- Frame 1726 to 1845
	13.	Attack1		- Frame 1846 to 1932
	14.	Attack2		- Frame 1933 to 2003
	15.	Death1		- Frame 2004 to 2063
	16.	Death2		- Frame 2067 to 2118
	17.	Death3		- Frame 2122 to 2188
	18.	Sprint		- Frame 2192.5 to 2217
	19.	Turn right	- Frame	2221 to 2262
	20.	Turn left	- Frame	2264 to 2307