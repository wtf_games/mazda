House Collection by Tronitec Game Studios

Contact:  contacts@tronitecgamestudios.com
Website:  http://www.tronitecgamestudios.com
EULA:  http://www.tronitecgamestudios.com/license.html

How To Use:
Drag any of the 180 prefabs, located in the 'House Collection/Prefabs' folder, to your project hierarchy to add a house to your scene.


Description:
A residential style home collection perfect for games and other real-time applications requiring a low polygon count. The houses contain 2 LODs (level of detail) for up close and far away viewing and a mirrored or flipped version of each house.


Items Included:
- 30 house models with 2 LODs for each house in (.fbx)
- 30 house models mirrored with 2 LODs for each house in (.fbx)
- 3 2048x2048 diffuse texture maps for each house in (.png)
- 3 materials for each house
- 180 prefabs encompassing all the house's texture and mirror configurations


Polygon (Quad) - Vertices Count:
House 1 LOD 1: 1236-1583
House 1 LOD 2: 414-637
House 2 LOD 1: 1124-1500
House 2 LOD 2: 431-727
House 3 LOD 1: 963-1264
House 3 LOD 2: 411-624
House 4 LOD 1: 776-1018
House 4 LOD 2: 332-504
House 5 LOD 1: 1119-1483
House 5 LOD 2: 396-623
House 6 LOD 1: 1062-1414
House 6 LOD 2: 426-674
House 7 LOD 1: 987-1329
House 7 LOD 2: 387-628
House 8 LOD 1: 1316-1754
House 8 LOD 2: 544-886
House 9 LOD 1: 998-1409
House 9 LOD 2: 355-628
House 10 LOD 1: 1315-1725
House 10 LOD 2: 475-812
House 11 LOD 1: 1061-1428
House 11 LOD 2: 357-608
House 12 LOD 1: 883-1156
House 12 LOD 2: 257-434
House 13 LOD 1: 902-1215
House 13 LOD 2: 346-1235
House 14 LOD 1: 1606-2048
House 14 LOD 2: 616-990
House 15 LOD 1: 1136-1585
House 15 LOD 2: 362-657
House 16 LOD 1: 1432-1883
House 16 LOD 2: 503-857
House 17 LOD 1: 954-1305
House 17 LOD 2: 397-681
House 18 LOD 1: 1074-1422
House 18 LOD 2: 381-649
House 19 LOD 1: 1287-1733
House 19 LOD 2: 477-819
House 20 LOD 1: 1101-1411
House 20 LOD 2: 382-694
House 21 LOD 1: 1357-1764
House 21 LOD 2: 448-739
House 22 LOD 1: 1298-1735
House 22 LOD 2: 502-814
House 23 LOD 1: 1392-1854
House 23 LOD 2: 504-877
House 24 LOD 1: 1263-1742
House 24 LOD 2: 516-868
House 25 LOD 1: 1080-1492
House 25 LOD 2: 338-621
House 26 LOD 1: 1130-1515
House 26 LOD 2: 366-644
House 27 LOD 1: 1050-1511
House 27 LOD 2: 424-797
House 28 LOD 1: 1144-1478
House 28 LOD 2: 423-734
House 29 LOD 1: 1168-1564
House 29 LOD 2: 448-793
House 30 LOD 1: 2074-2705
House 30 LOD 2: 797-1319