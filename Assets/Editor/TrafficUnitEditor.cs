﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TrafficUnit))]
public class TrafficUnitEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		TrafficUnit trafficUnit = (TrafficUnit)target;
		if(GUILayout.Button("Get Lenght", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
			trafficUnit.unitLenght = trafficUnit.transform.GetComponent<BoxCollider>().size.z;
		}
	}
}
