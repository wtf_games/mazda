﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GameCanvase))]
public class GameCanvaseEditor : Editor {
	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		GameCanvase gameCanvase = (GameCanvase)target;

		if(GUILayout.Button("Screen Flash White", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
			gameCanvase.ScreenFlashWhite();
		}
	}
}
