﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlayerCarsManager))]
public class PlayerCarsManagerEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		PlayerCarsManager playerCarsManager = (PlayerCarsManager)target;

		if(GUILayout.Button("Previous Car", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
			playerCarsManager.SetNewPlayerCar(0);
		}

		if(GUILayout.Button("Next Car", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
			playerCarsManager.SetNewPlayerCar(1);
		}
	}

}
