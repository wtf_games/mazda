﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TrafficManager))]
public class TrafficManagerEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		TrafficManager trafficManager = (TrafficManager)target;
		if(GUILayout.Button("Update Traffic Lines", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
			//trafficManager.UpdateTrafficLines();
		}
	}
}