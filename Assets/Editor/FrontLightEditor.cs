﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CarLight))]
public class FrontLightEditor : Editor {

	void OnSceneGUI() {
		CarLight fow = (CarLight)target;
		Handles.color = Color.white;
		Handles.DrawWireArc (fow.transform.position, Vector3.up, Vector3.forward, 360, fow.lightMeshRadius);
		Vector3 viewAngleA = fow.DirFromAngle (-fow.lightMeshAngle / 2, false);
		Vector3 viewAngleB = fow.DirFromAngle (fow.lightMeshAngle / 2, false);

		Handles.DrawLine (fow.transform.position, fow.transform.position + viewAngleA * fow.lightMeshRadius);
		Handles.DrawLine (fow.transform.position, fow.transform.position + viewAngleB * fow.lightMeshRadius);
	}
}
