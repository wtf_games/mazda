﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TrackControllsPair : MonoBehaviour {

	public Transform leftTrackControll;
	public Transform rightTrackControll;

	public float center;
	public float angle;

	void Start() {
		center = (leftTrackControll.position.x + rightTrackControll.position.x) * 0.5f;
		angle = (leftTrackControll.rotation.y + rightTrackControll.rotation.y) * 0.5f;
	}
}
