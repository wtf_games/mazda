﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RoadSegment : MonoBehaviour, IPoolable
{
    public Transform stratPoint;
    public Transform endPoint;

	public List<GameObject> cars = new List<GameObject>();
	public List<GameObject> _actual = new List<GameObject>();
    public List<Transform> wayPoints = new List<Transform>();

    private int _checkPointCount = 0;
    private int _collisionPointCount = 0;

    private Pool _pool;

	[SerializeField]
	private GameTypeEnum currentGameType;

	public bool reactByTrigger;

	private GameObject CarAI;

	public int markersCount;

    public void Init(Pool pool)
    {
        _pool = pool;
		currentGameType = Core.Instance.GetService<IGameManager>().CurrentGameType;
		switch (currentGameType)
		{
			case GameTypeEnum.ALH:
				//InstantiateCars("RoadPrefabs/" + currentGameType);
				break;
			case GameTypeEnum.LRC:
				break;
			case GameTypeEnum.CTJ:
				break;
			case GameTypeEnum.HDT:
				break;
		}
    }

    void OnTriggerEnter(Collider other) {
		if(reactByTrigger) {
			if (other.tag == "PlayerCar") {
				_pool.NextRoadSegment();
			}

			if (other.tag == "CameraTrigger") {
				Deactivate();
			}
		} else {
			return;
		}
    }

    public void ActivateCars(List<Transform> pos, int count)
    {
        for (int i = 0; i < count; i++) {
			var obj = TrafficManager.instance.GetTrafficUnit();
			obj.transform.position = pos[i].position;
			obj.transform.rotation = pos[i].rotation;
			obj.gameObject.SetActive(true);
			obj.name = string.Format("CarAi-{0}{1}",(i + 1), GetInstanceID());
        }
    }

	public void Reuse(Vector3 position, Quaternion rotation) {
		transform.position = position;
		transform.rotation = rotation;
		gameObject.SetActive(true);

		switch (currentGameType)
		{
			case GameTypeEnum.ALH:
				ActivateCars(wayPoints, wayPoints.Count);
				break;
			case GameTypeEnum.LRC:
				break;
			case GameTypeEnum.CTJ:
				break;
			case GameTypeEnum.HDT:
				break;
		}
	}

	public void Deactivate() {
		gameObject.SetActive(false);
		_pool.activeRoadSegments.Remove(this);
		_pool.allRoadSegments.Add(this);
	}
}
