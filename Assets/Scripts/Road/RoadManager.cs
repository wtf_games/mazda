﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadManager : MonoBehaviour
{
    public GameTypeEnum gameType;

	public List<RoadSegment> roadSegmentsList = new List<RoadSegment>();

    private GameObject _poolType;
    private GameObject _car;
    private GameObject _AICar;

    private Dictionary<string, int> _parts;
	private List<string> _partsList = new List<string>();

    private Pool _pool;
    private GameObject roadGenerator;


    void Start()
    {
		CreateRoad(gameType);
    }

    public void CreateRoad(GameTypeEnum gameType)
    {
        _poolType = new GameObject();
        _poolType.name = "Pool-RoadParts";
        _pool = _poolType.AddComponent<Pool>();
        _parts = new Dictionary<string, int>();

        switch (gameType)
        {
            case GameTypeEnum.ALH:
				_partsList.Add("straight-section");
				_partsList.Add("straight-section-house");
				_partsList.Add("straight-section");
				_partsList.Add("straight-section");
				_pool.Init("RoadPrefabs/ALH", _partsList, 2);

                break;
            case GameTypeEnum.LRC:
				_partsList.Add("straight-section");
				_partsList.Add("curved-a");
				_partsList.Add("straight-section");
				_partsList.Add("curved-a-deers");
				_partsList.Add("straight-section");
				_pool.Init("RoadPrefabs/LRC", _partsList, 3);
                break;
            case GameTypeEnum.CTJ:

                break;
            case GameTypeEnum.HDT:

                break;
            default:
                 
                break;
        }

//		roadSegmentsList.Clear();
//       
//		var first = _pool.GetNextObject<RoadSegment>(new Vector3(0, 0, -50f), Quaternion.identity);
//        first.NextSegment();
//
//		var second = _pool.GetNextObject<RoadSegment>(first.endPoint.position, first.endPoint.rotation);
//		second.NextSegment();
    }

    public void DestroyRoad()
    {
        GameObject.Destroy(_pool.gameObject);
        Camera.main.transform.SetParent(GameObject.Find("Core").transform);
        Camera.main.transform.localPosition = new Vector3(0, 0, -10f);
        Camera.main.transform.localRotation = Quaternion.Euler(0, 0, 0);
        GameObject.Destroy(_car.gameObject);
        GameObject.Destroy(roadGenerator.gameObject);
    }
}