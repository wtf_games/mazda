﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WayPoints
{
    public List<Transform> wayPoints = new List<Transform>();
    public GameObject[] roadsPieces;

   
    public void GetRoadPieces()
    {
        roadsPieces = GetPiecesArray();

        for (int i = 0; i < roadsPieces.Length; i++)
        {
            GameObject _point = GameObject.Instantiate(Resources.Load("RoadPrefabs/ALH/Sphere"),
                                                    new Vector3(roadsPieces[i].transform.position.x,
                                                    roadsPieces[i].transform.position.y + 1,
                                                    roadsPieces[i].transform.position.z),
                                                    roadsPieces[i].transform.rotation) as GameObject;
            _point.transform.SetParent(roadsPieces[i].transform);
            _point.transform.localPosition = new Vector3(-4, 2, 0);
            _point.transform.localRotation = roadsPieces[i].transform.rotation;
        }
    }

    public GameObject[] GetPiecesArray()
    {
        roadsPieces = GameObject.FindGameObjectsWithTag("RoadPiece");
        return roadsPieces;
    }

    public List<Transform> GetPoints()
    {
        for (int i = 0; i < GetPiecesArray().Length; i++)
        {
            var point = roadsPieces[i].transform.GetChild(0);
            wayPoints.Add(point);
        }
        return wayPoints;
    }
}
