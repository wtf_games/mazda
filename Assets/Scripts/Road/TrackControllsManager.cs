﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrackControllsManager : MonoBehaviour {

	public static TrackControllsManager instance;

	public List<TrackControllsPair> trackControllsPairsList = new List<TrackControllsPair>();

	void Awake() {
		instance = this;
	}

	void Start () {
		trackControllsPairsList.Clear();
		for(int i = 0; i < transform.childCount; i++) {
			trackControllsPairsList.Add(transform.GetChild(i).GetComponent<TrackControllsPair>());
		}
	}
}
