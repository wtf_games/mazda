﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class GameCanvase : MonoBehaviour
{
	public static GameCanvase Instance;

	//Generic UI Elements
	public GameObject topPanel;
	public GameObject bottomPanel;
	public CanvasGroup fadeImage;
	public CanvasGroup screenFlashCanvasGroup;
    private Camera m_GameCamera;
    private RectTransform controllPanel;
    private Transform lineDetector;
    public GameObject follPanel;
    public GameObject mazdaPanel;
    public CanvasGroup carAIPanel;
    public GameObject finishPanel;
	[SerializeField]
    private CarController carControll;
    bool isLeft;
    bool isRight;

	private bool isScreenFlashed = false;
	[SerializeField]
	private GameTypeEnum currentGameType;

	void Awake() {
		Instance = this;
	}

    void Start()
    {
		StartCoroutine("ScreeFadeOut");
		currentGameType = Core.Instance.GetService<IGameManager>().CurrentGameType;

		switch (currentGameType)
        {
            case GameTypeEnum.ALH:
                lineDetector = GameObject.FindGameObjectWithTag("LineDetector").transform;
                break;
            case GameTypeEnum.LRC:
                break;
            case GameTypeEnum.CTJ:
                break;
            case GameTypeEnum.HDT:
                break;
        }

        controllPanel = this.transform.FindChild("Buttons Panel").GetComponent<RectTransform>();

		m_GameCamera = GameObject.FindGameObjectWithTag("GameCamera").GetComponent<Camera>();
		
		carControll = PlayerCarsManager.Instance.carController;
    }

    public void Left()
    {
		if(carControll.canTurnLeft) {
			carControll.TurnCarLeft();	
		}
    }

    public void Right()
    {
		if(carControll.canTurnRight) {
			carControll.TurnCarRight();
		}
    }

	IEnumerator TurnCar(float targetRotation) {
		Quaternion newRotation = Quaternion.Euler(0, targetRotation, 0);

		float turnSpeed = 3;
		float percent = 0;

		while(percent < 1f) {
			percent += Time.deltaTime * turnSpeed;
			float interpolation = (-Mathf.Pow(percent,2) + percent) * 4;
			carControll.transform.rotation = Quaternion.Lerp(carControll.transform.rotation, newRotation, interpolation);
			yield return null;
		}

		//carControll.transform.rotation = Quaternion.Slerp(carControll.transform.rotation, , Time.deltaTime * 2f);
	}

    public void UP()
    {
        isRight = false;
        isLeft = false;
        carControll.handBreak = 0f;
    }

    public void Game()
    {
        Time.timeScale = 1;
        follPanel.SetActive(false);
    }

    public void OK()
    {
		Time.timeScale = 1;
		finishPanel.SetActive(false);
//		SceneManager.UnloadScene(Core.Instance.GetService<IGameManager>().CurrentGameType.ToString());
//		Core.Instance.GetService<IStateMachine>().SwitchState(StatesEnum.Menu);
		SceneManager.LoadScene(0);
    }

	public void OnRestartButtonClick() {
		Time.timeScale = 1;
		StartCoroutine("ScreeFadeIn");
		finishPanel.SetActive(false);
	}

    public void TryMazdaMode()
    {
        Time.timeScale = 1;
        mazdaPanel.SetActive(false);
        StartCoroutine(MazdaMode());
    }

    IEnumerator MazdaMode()
    {
		PlayerCarsManager.Instance.SetNewPlayerCar(1);
		carControll = PlayerCarsManager.Instance.carController;
		PlayerCarsManager.Instance.carLightsController.autoLight = true;
        while (true)
        {
            yield return new WaitForSeconds(30f);
            StopCoroutine(MazdaMode());
			PlayerCarsManager.Instance.carLightsController.autoLight = false;
            Time.timeScale =0 ;
            finishPanel.SetActive(true);
        }
    }

    public void ScreenFlashWhite()
    {
		if(!isScreenFlashed) {
			StartCoroutine("DoFlashWhite");	
		}
    }

    IEnumerator DoFlashWhite()
    {
		float flashSpeed = 3f;
		isScreenFlashed = true;
		while (screenFlashCanvasGroup.alpha < 1)
        {
			screenFlashCanvasGroup.alpha += Time.deltaTime * flashSpeed;
            yield return null;
        }
		while (screenFlashCanvasGroup.alpha > 0)
        {
			screenFlashCanvasGroup.alpha -= Time.deltaTime * flashSpeed;
            yield return null;
        }
		isScreenFlashed = false;
    }

    public void Brake()
    {
        carControll.handBreak = 1000f;
    }

	public void ToggleLightsMode() {
		if(carControll.GetComponent<CarLightsController>() != null) {
			if(carControll.GetComponent<CarLightsController>().currentLightMode == CarLightsController.lightModes.Far) {
				carControll.GetComponent<CarLightsController>().TurnNearLight();
			} else {
				carControll.GetComponent<CarLightsController>().TurnFarLight();
			}
		}
	}


    bool isFullScreen;
    public void FullScreenMode()
    {
        isFullScreen = !isFullScreen;
        if (isFullScreen)
        {
            m_GameCamera.rect = new Rect(0, 0, 1, 1);
			topPanel.SetActive(false);
			bottomPanel.SetActive(false);
        }
        else
        {
			m_GameCamera.rect = new Rect(0, 0.07f, 1f, 0.75f);
			topPanel.SetActive(true);
			bottomPanel.SetActive(true);
        }
        
    }

	IEnumerator ScreeFadeIn() {
		fadeImage.alpha = 0f;
		while(fadeImage.alpha < 1f) {
			yield return new WaitForSeconds(0.01f);
			fadeImage.alpha += 0.1f;
		}

		SceneManager.UnloadScene(Core.Instance.GetService<IGameManager>().CurrentGameType.ToString());
		var go = GameObject.Find("Pool-RoadParts");
		Destroy(go);
		Core.Instance.GetService<IStateMachine>().SwitchState(StatesEnum.Game);
		SceneManager.LoadScene(Core.Instance.GetService<IGameManager>().CurrentGameType.ToString());
	}

	IEnumerator ScreeFadeOut() {
		fadeImage.alpha = 1f;
		while(fadeImage.alpha > 0) {
			yield return new WaitForSeconds(0.01f);
			fadeImage.alpha -= 0.1f;
		}
	}
}
