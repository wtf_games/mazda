﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour {

	private AudioSource _audio;
	void Awake () {
		_audio = GetComponent<AudioSource>();
		#if UNITY_EDITOR
		_audio.Play();
		#else
		_audio.Stop();
		#endif
	}
}
