﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
	private Transform _target;
    private Vector3 _velicity = Vector3.zero;
    private float _smooth = 0.5f;


	void FixedUpdate ()
    {
		transform.LookAt(_target.position);

		if(Vector3.Distance(transform.position, _target.position) > 18f)
        {
			Vector3 targetPos = new Vector3(_target.position.x, Mathf.Clamp(transform.position.y, 15f, 20f), _target.position.z);
			transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref _velicity, _smooth);
		}
	}
}
