﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TraficManager
{
    public List<GameObject> cars = new List<GameObject>();
    public List<GameObject> _actual = new List<GameObject>();

    private GameObject CarAI;

    public void Init(string path)
    {
        CarAI = Resources.Load(path + "/" + "CarAI") as GameObject;

        for (int i = 0; i < 4; i++)
        {
            var go = GameObject.Instantiate(CarAI) as GameObject;
            cars.Add(go);
            cars[i].SetActive(false);
        }
       
    }

    public void InstantiateCar(List<Transform> pos,int count)
    {
        for (int i = 0; i < count; i++)
        {
            var obj = cars[0];
            cars.Remove(obj);
            _actual.Add(obj);
            if (pos[i].position.x > 0)
            {
                obj.transform.rotation = new Quaternion();
                obj.transform.position = pos[i].position;
                obj.gameObject.SetActive(true);
            }
            else
            {
                obj.transform.position = pos[i].position;
                obj.gameObject.SetActive(true);
            }
        }
    }

    public void DeactivateCar(List<GameObject> obj, List<GameObject> car)
    {
        for (int i = 0; i < obj.Count; i++)
        {
            obj[i].SetActive(false);
            _actual.Remove(obj[i]);
            car.Add(obj[i]);
        }
    }
}
