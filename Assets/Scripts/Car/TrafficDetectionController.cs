﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrafficDetectionController : MonoBehaviour {

	public ParticleSystem waves;

	private CarController carController;
	private AICarController aiCarController;

	private AICarController hitAICarController;

	public enum controllerType {
		AI,
		Palyer
	}

	public controllerType currentControllerType;

	private float prevSpeed;
	private float newSpeed;

	//0 - FL Ray
	//1 - FR Ray
	//2 - FRS Ray
	//3 - RRS Ray
	//4 - LLS Ray
	//5 - LFS Ray

	public List<TrafficDetectionRay> trafficDetectionRaysList = new List<TrafficDetectionRay>();

	void Awake () {
		if(currentControllerType == controllerType.Palyer) {
			carController = transform.parent.GetComponent<CarController>();
			prevSpeed = carController.MaxSpeed;
		} else {
			aiCarController = GetComponent<AICarController>();
			prevSpeed = aiCarController.MaxSpeed;
		}

		if(waves != null) {
			waves.Stop();	
		}
	}

	void Update() {
		MakeRayCats();
	}

	void OnDrawGizmos() {
		for (int i = 0; i < trafficDetectionRaysList.Count; i++) {
			Vector3 rayOrigin = transform.TransformPoint(trafficDetectionRaysList[i].rayPosition);
			Vector3 rayDirection = transform.TransformDirection(trafficDetectionRaysList[i].rayDirection);
			Ray ray = new Ray(rayOrigin, rayDirection);
			if(currentControllerType == controllerType.Palyer) {
				Gizmos.color = Color.yellow;	
			} else {
				Gizmos.color = Color.red;
			}
			Gizmos.DrawRay(ray.origin, ray.direction * trafficDetectionRaysList[i].rayDistance);
		}
	}

	public void MakeRayCats() {
		for (int i = 0; i < trafficDetectionRaysList.Count; i++) {

			Vector3 rayOrigin = transform.TransformPoint(trafficDetectionRaysList[i].rayPosition);
			Vector3 rayDirection = transform.TransformDirection(trafficDetectionRaysList[i].rayDirection);

			Ray ray = new Ray(rayOrigin, rayDirection);
			RaycastHit hit = new RaycastHit();

			if(Physics.Raycast(ray, out hit, trafficDetectionRaysList[i].rayDistance)) {
				if(hit.collider.tag == "CarAI") {
					Debug.Log(i);
					hitAICarController = hit.collider.GetComponent<AICarController>();
					newSpeed = hitAICarController.CurrentSpeed;
					//Define Logic
					DefineLogic(i);

					//Draw visual indication
					if(waves != null) {

						Vector3 direction = hit.point - waves.transform.position;
						waves.transform.rotation = Quaternion.LookRotation(direction);

						if(waves.isStopped || waves.isPaused) {
							waves.IsAlive();
							waves.Play();
						}
					}
				}
			}
		}
	}

	void DefineLogic(int raycatsID) {
		if(currentControllerType == controllerType.Palyer) {
			switch(raycatsID) {
				case 0:
					carController.MaxSpeed = newSpeed;
					hitAICarController.MaxSpeed += 2f;
					carController.canTurnLeft = true;
					carController.canTurnRight = true;							
					break;
				case 1:
					carController.MaxSpeed = newSpeed;
					hitAICarController.MaxSpeed += 2f;
					carController.canTurnLeft = true;
					carController.canTurnRight = true;
					break;
				case 2:
					carController.canTurnLeft = true;
					carController.canTurnRight = false;
					break;
				case 3:
					carController.canTurnLeft = true;
					carController.canTurnRight = false;
					break;
				case 4:
					carController.canTurnLeft = false;
					carController.canTurnRight = true;
					break;
				case 5:
					carController.canTurnLeft = false;
					carController.canTurnRight = true;
					break;
				default:
					carController.MaxSpeed = prevSpeed;
					carController.canTurnLeft = true;
					carController.canTurnRight = true;
					break;
			}

			StartCoroutine("RestorePlayerCarBehaviour");
		} else {
			if(raycatsID == 0) {
				aiCarController.MaxSpeed = newSpeed;
				aiCarController.ChangeTrafficLine();
				StartCoroutine("RestoreAICarBehaviour");
			}
		}		
	}

	IEnumerator RestorePlayerCarBehaviour() {
		yield return new WaitForSeconds(2f);
		carController.MaxSpeed = prevSpeed;
		carController.canTurnLeft = true;
		carController.canTurnRight = true;
	}

	IEnumerator RestoreAICarBehaviour() {
		yield return new WaitForSeconds(2f);
		aiCarController.MaxSpeed = prevSpeed;
	}
}