﻿using UnityEngine;
using System;
using System.Collections;

public class CarLightsController : MonoBehaviour {

	public CarLight lightFL;
	public CarLight lightFR;

	public float nearLightRange = 20f;
	public float farLightRange = 100f;

	public Transform lightDetectorTrigger;

	public bool autoLight = false;

	public enum lightModes {
		Near,
		Far
	}

	public lightModes currentLightMode = lightModes.Far;

	void Start() {
		TurnNearLight();
	}

	void Update () {

		if(Input.GetKeyDown(KeyCode.L)) {
			lightFL.ToggleLight();
			lightFR.ToggleLight();
		}

		if(Input.GetKeyDown(KeyCode.Space)) {
			if(currentLightMode == lightModes.Near) {
				TurnFarLight();	
			} else {
				TurnNearLight();	
			}
		}
	}

	public void TurnNearLight()
	{
		currentLightMode = lightModes.Near;
		lightFL.light.range = nearLightRange;
		lightFR.light.range = nearLightRange;
	}

	public void TurnFarLight()
	{
		currentLightMode = lightModes.Far;
		lightFL.light.range = farLightRange;
		lightFR.light.range = farLightRange;
	}
}
