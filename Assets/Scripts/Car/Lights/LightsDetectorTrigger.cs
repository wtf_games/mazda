﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LightsDetectorTrigger : MonoBehaviour {

	public List<GameObject> carsList = new List<GameObject>();
	public float lightThreshlod = 35f;

	private BoxCollider m_LightTrigger;
	private CarLightsController _carLightsController;

	public void Init () {
		_carLightsController = PlayerCarsManager.Instance.carLightsController;

		m_LightTrigger = GetComponent<BoxCollider>();

		if(!m_LightTrigger.isTrigger) {
			m_LightTrigger.isTrigger = true;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "CarAI")
		{
			carsList.Add(other.gameObject);
			foreach (var item in carsList)
			{
				if (item.transform.position.z - transform.position.z <= lightThreshlod)
				{
					if(_carLightsController.autoLight) {
						_carLightsController.TurnNearLight();
					} else {

						float angle = Vector3.Angle(Vector3.forward, item.transform.forward);

						#if UNITY_EDITOR
						Debug.Log(item.name + " Angle: " + angle);
						#endif

						if(angle == 180f && _carLightsController.currentLightMode == CarLightsController.lightModes.Far) {
							GameCanvase.Instance.ScreenFlashWhite();
						}
					}
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "CarAI")
		{
			RemoveCarFromList(other.gameObject);
		}
	}

	public void RemoveCarFromList(GameObject carToRemove) {
		if(carsList.Contains(carToRemove)) {
			carsList.Remove(carToRemove);
			if(carsList.Count == 0) {
				if(_carLightsController.autoLight) {
					_carLightsController.TurnFarLight();	
				}
			}
		}
	}
}
