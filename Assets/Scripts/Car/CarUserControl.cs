using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class CarUserControl : MonoBehaviour
    {
    //    [HideInInspector]
    //    public float handBreak;
    //    public GameObject follPanel;
    //    public GameObject mazdaPanel;
    //    public CanvasGroup carAIPanel;
    //    public GameObject finishPanel;
    //    private RectTransform pan;
    //    private float fadeTime = 0.5f;

    //    public RectTransform CanvasRect;
    //    private Camera cam;
    //    [HideInInspector]
    //    public CarController m_Car; // the car controller we want to use

    //    public bool isLight;

    //    private Transform lineDetector;
    //    Quaternion rotation;

    //    private void Awake()
    //    {
    //        // get the car controller
    //        m_Car = GetComponent<CarController>();
    //    }

    //    void Start()
    //    {
    //        cam = GameObject.Find("GameCamera").GetComponent<Camera>();
    //        pan = carAIPanel.GetComponent<RectTransform>();
    //        lineDetector = GameObject.Find("LineDetector").transform;
    //    }


    //    private void FixedUpdate()
    //    {
    //        Vector3 relativePos = lineDetector.position - transform.position;
    //        rotation = Quaternion.LookRotation(relativePos);
    //        transform.rotation = transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime / 0.7f);
    //    }

    //    bool near = false;

    //    void Update()
    //    {

    //        if (Input.GetKeyDown(KeyCode.F))
    //        {
    //            m_Car.TurnFarLight();
    //            near = false;
    //        }
    //        if (Input.GetKeyDown(KeyCode.N))
    //        {
    //            foreach (var item in m_Car.car)
    //            {
    //                if (item.transform.position.z - transform.position.z <= 35)
    //                {

    //                    StartCoroutine(Fade());
    //                }
    //            }
    //            m_Car.TurnNearLight();
    //            near = true;
    //        }

    //        if (isLight == true)
    //        {
    //            m_Car.TurnNearLight();
    //            near = true;
    //        }

    //        if (near)
    //        {
    //            foreach (var item in m_Car.car)
    //            {
    //                if (item.transform.position.z - transform.position.z <= -10)
    //                {
    //                    m_Car.TurnFarLight();
    //                    near = false;
    //                    isLight = false;
    //                }
    //            }
    //        }
            

    //        if (m_Car.isCrash)
    //        {
    //            m_Car.isCrash = false;
    //            follPanel.SetActive(true);
    //            Time.timeScale = 0;
    //            m_Car.collCar.SetActive(false);
    //            if (m_Car.follCount == 2)
    //            {
    //                follPanel.SetActive(false);
    //                mazdaPanel.SetActive(true);
    //                m_Car.follCount = 0;
    //            }
    //        }

    //        if (Input.GetKeyDown(KeyCode.A))
    //        {
    //            m_Car.isAutoLight = true;
    //        }
    //        m_Car.Move(rotation.y / 20, 1, 0, handBreak);
    //        if (m_Car.car != null && m_Car.isAutoLight)
    //        {
    //            foreach (GameObject item in m_Car.car)
    //            {
    //                if (item.transform.position.z - transform.position.z <= 40)
    //                {
    //                    m_Car.TurnNearLight();
    //                }
    //                if (item.transform.position.z - transform.position.z <= -10)
    //                {
    //                    m_Car.TurnFarLight();
    //                }
    //            }

    //        }
    //    }

    //    IEnumerator Fade()
    //    {
    //        carAIPanel.alpha = 1;
    //        while (carAIPanel.alpha > 0)
    //        {
    //            carAIPanel.alpha -= fadeTime * Time.deltaTime;
    //            yield return null;
    //        }
    //    }

    }
}
