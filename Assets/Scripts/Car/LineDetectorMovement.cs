﻿using UnityEngine;
using System.Collections;

public class LineDetectorMovement : MonoBehaviour
{
    public GameObject car;

	void Awake() {
		if(car == null) {
			car = GameObject.FindGameObjectWithTag("Player");
		}
	}

    void Update()
    {
        transform.position = new Vector3(transform.position.x, car.transform.position.y, car.transform.position.z + 60);
    }
}
