﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TrafficDetectionRay {

	public Vector3 rayPosition;
	public Vector3 rayDirection;
	public float rayDistance = 1f;

	public TrafficDetectionRay(Vector3 rayPosition, Vector3 rayDirection, float rayDistance) {
		this.rayPosition = rayPosition;
		this.rayDirection = rayDirection;
		this.rayDistance = rayDistance;
	}
}
