﻿using UnityEngine;
using System.Collections;

public class AICarDetector : MonoBehaviour {

	private AICarController aiCarController;

	void Awake () {
		aiCarController = GetComponent<AICarController>();
	}

	void OnEnable() {
		//StartCoroutine("CheckAiCars");
	}

	void OnDisable() {
		//StopAllCoroutines();
	}

	IEnumerator CheckAiCars () {
		while(true) {
			yield return new WaitForSeconds(5f);
			Debug.Log("CheckAiCars");
			Collider[] hitColliders = Physics.OverlapSphere(transform.position, 3f);
			for(int i = 0; i < hitColliders.Length; i++) {
				if(hitColliders[i].tag == "CarAI" && hitColliders[i] != GetComponent<Collider>()) {
					float distance = Vector3.Distance(transform.position, hitColliders[i].transform.position);
					float angle = Vector3.Angle(transform.forward, hitColliders[i].transform.forward);
					Debug.Log("Distance: " + distance + ", Angle: " + angle);
					if(distance < 10f && angle < 90f) {
						TrafficManager.instance.TakeTrafficUnit(hitColliders[i].gameObject);
					}
				}
			}
		}
	}
}
