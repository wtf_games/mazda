﻿public interface IPoolable
{
    void Init(Pool pool);
}
