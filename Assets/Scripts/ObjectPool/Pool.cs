﻿using UnityEngine;
using System.Collections.Generic;

public class Pool : MonoBehaviour
{
	public List<RoadSegment> allRoadSegments = new List<RoadSegment>();
	public List<RoadSegment> activeRoadSegments = new List<RoadSegment>();

	public RoadSegment currentRoadSegment;

	public void Init(string path, List<string> prefabNames, int activeSegmentsCount = 0)
    {
		for (int i = 0; i < prefabNames.Count; i++) {
			var go = GameObject.Instantiate(Resources.Load(path + "/" + prefabNames[i])) as GameObject;
			RoadSegment component = go.GetComponent<RoadSegment>();
			component.Init(this);
			allRoadSegments.Add(component);
			go.transform.SetParent(this.transform);
			go.SetActive(false);
		}

		for (int i = 0; i < activeSegmentsCount; i++) {
			currentRoadSegment = allRoadSegments[0];
			allRoadSegments.Remove(currentRoadSegment);
			activeRoadSegments.Add(currentRoadSegment);
			if(i == 0) {
				currentRoadSegment.Reuse(new Vector3(0,0,-10f), Quaternion.identity);
			} else {
				currentRoadSegment.Reuse(activeRoadSegments[i - 1].endPoint.position, activeRoadSegments[i - 1].endPoint.rotation);
			}
		}
    }

	public void NextRoadSegment() {
		RoadSegment nextRoadSegment = allRoadSegments[0];
		allRoadSegments.Remove(nextRoadSegment);
		nextRoadSegment.Reuse(currentRoadSegment.endPoint.position, currentRoadSegment.endPoint.rotation);
		activeRoadSegments.Add(nextRoadSegment);
		currentRoadSegment = nextRoadSegment;
	}

//	public void InitWithList<T>(string path, List<string> prefabNames, int activeSegmentsCount = 0) where T : Component, IPoolable {
//		for (int i = 0; i < prefabNames.Count; i++) {
//			var go = GameObject.Instantiate(Resources.Load(path + "/" + prefabNames[i])) as GameObject;
//			var component = go.GetComponent<T>();
//			component.Init(this);
//			_allPosible.Add(component);
//			go.transform.SetParent(this.transform);
//			go.SetActive(false);
//		}
//
//		for (int i = 0; i < prefabNames.Count; i++) {
//			var obj = _allPosible[i];
//			currentSegmentIndex = i;
//			if(i == 0) {
//				GetNextObject<RoadSegment>(new Vector3(0,0,-50f), Quaternion.identity);	
//			} else {
//				
//			}
//		}
//	}
//
//	public T GetNextObject<T>(Vector3 pos, Quaternion rot, string objectName = null) where T : Component
//    {
//		var obj = _allPosible[currentSegmentIndex];
//        _allPosible.Remove(obj);
//        _actual.Add(obj);
//        obj.transform.position = pos;
//		obj.transform.rotation = rot;
//        obj.gameObject.SetActive(true);
//        return (T)obj;
//    }
//
//    public void Deactivate<T>(T obj) where T : Component
//    {
//        obj.gameObject.SetActive(false);
//        _actual.Remove(obj);
//        _allPosible.Add(obj);
//    }
}
