﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCarsManager : MonoBehaviour {

	public static PlayerCarsManager Instance;
	public List<GameObject> playerCars = new List<GameObject>();

	private GameObject _currentPlayerCar;
	public GameObject currentPlayerCar {
		get {
			return _currentPlayerCar;
		}

		set {
			_currentPlayerCar = value;
			carController = _currentPlayerCar.GetComponent<CarController>();
			carLightsController = _currentPlayerCar.GetComponent<CarLightsController>();
			lightsDetectorTrigger = _currentPlayerCar.GetComponentInChildren<LightsDetectorTrigger>();
		}
	}


	public CarController carController;
	public CarLightsController carLightsController;
	public LightsDetectorTrigger lightsDetectorTrigger;

	private Vector3 newCarPosition = Vector3.zero;
	private Quaternion newCarRotation = Quaternion.identity;

	void Awake() {
		Instance = this;
		Init();
	}

	void FixedUpdate() {
		//transform.position = currentPlayerCar.transform.position;
	}

	void Init() {
		SetNewPlayerCar(0);
	}

	public void SetNewPlayerCar(int carID) {

		if(currentPlayerCar != null) {

			if(carController.desiredPositon != Vector3.zero) {
				newCarPosition = carController.desiredPositon;	
			} else {
				newCarPosition = currentPlayerCar.transform.position;
			}

			newCarRotation = currentPlayerCar.transform.rotation;
		} else {
			newCarPosition = new Vector3(1.75f, 0f, 0f);
		}

		for(int i = 0; i < transform.childCount; i++) {
			Destroy(transform.GetChild(i).gameObject);
		}

		GameObject playerCar = Instantiate(playerCars[carID], newCarPosition, newCarRotation) as GameObject;
		playerCar.transform.SetParent(transform);
		currentPlayerCar = playerCar;
		lightsDetectorTrigger.Init();
	}
}
