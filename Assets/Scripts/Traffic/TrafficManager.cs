﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrafficManager : MonoBehaviour {

	public static TrafficManager instance;

	public List<GameObject> inactiveTrafficUnitsList = new List<GameObject>();
	public List<GameObject> activeTrafficUnitsList = new List<GameObject>();

	public bool checkDistance = true;

	void Awake() {
		instance = this;
		Init ();
	}

	void Start () {
		StartCoroutine("CheckDistanceCoroutine");
	}

	IEnumerator CheckDistanceCoroutine() {
		while(checkDistance) {
			yield return new WaitForSeconds(5f);
			for(int i = 0; i < activeTrafficUnitsList.Count; i++) {
				GameObject trafficUnit = activeTrafficUnitsList[i];
				if(Vector3.Distance(trafficUnit.transform.position, PlayerCarsManager.Instance.currentPlayerCar.transform.position) > 200f) {
					if(activeTrafficUnitsList.Contains(trafficUnit)) {
						TakeTrafficUnit(trafficUnit);
					}
				}
			}
		}
	}

	void Init () {
		CreateTrafficUnits("RoadPrefabs/ALH", 30);
	}

	void CreateTrafficUnits(string path, int unitsCount) {

		inactiveTrafficUnitsList.Clear();

		for(int i = 0; i < unitsCount; i++) {
			GameObject newTrafficUnit = GameObject.Instantiate(Resources.Load(path + "/CarAI")) as GameObject;
			newTrafficUnit.transform.SetParent(this.transform);
			newTrafficUnit.gameObject.SetActive(false);
			inactiveTrafficUnitsList.Add(newTrafficUnit);
		}
	}

	public GameObject GetTrafficUnit() {
		GameObject unit = inactiveTrafficUnitsList[0];
		inactiveTrafficUnitsList.Remove(unit);
		activeTrafficUnitsList.Add(unit);
		return unit;
	}

	public void TakeTrafficUnit(GameObject unit) {
		activeTrafficUnitsList.Remove(unit);
		unit.transform.position = transform.position;
		unit.SetActive(false);
		if(!inactiveTrafficUnitsList.Contains(unit)) {
			inactiveTrafficUnitsList.Add(unit);	
		}
	}
}
