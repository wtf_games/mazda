﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TrafficLine {
	public Vector3 startPoint;
	public Vector3 endPoint;
	[Range(0f, 1f)]
	public float loadingPecent;
	public List<TrafficUnit> lineUnitsList = new List<TrafficUnit>();

	private int unitsCount;

	public TrafficLine(Vector3 startPoint, Vector3 endPoint, float loadingPecent) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.loadingPecent = loadingPecent;
	}

	public void InstantiateCars() {
		lineUnitsList.Clear();
		unitsCount = Mathf.RoundToInt((Vector3.Distance(startPoint, endPoint) * loadingPecent));
		Debug.Log("Units Count: " + unitsCount);

		for(int i = 0; i < unitsCount; i++) {
			#if UNITY_EDITOR
			GameObject tup = UnityEditor.PrefabUtility.InstantiatePrefab(UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/RoadPrefabs/ALH/CarAI.prefab")) as GameObject;
			TrafficUnit newTrafficUnit = tup.GetComponent<TrafficUnit>();
			#else
			TrafficUnit newTrafficUnit = GameObject.Instantiate(Resources.Load("RoadPrefabs/ALH/CarAI")) as TrafficUnit;
			#endif
			Vector3 newTrafficUnitPosiiton = new Vector3(startPoint.x, 0f, i * newTrafficUnit.unitLenght);
			Debug.Log(newTrafficUnitPosiiton);
			newTrafficUnit.transform.position = newTrafficUnitPosiiton;
			newTrafficUnit.transform.rotation = Quaternion.identity;
			lineUnitsList.Add(newTrafficUnit);
		}
	}
}