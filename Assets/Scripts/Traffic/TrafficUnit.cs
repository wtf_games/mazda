﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class TrafficUnit : MonoBehaviour {

	private GameObject localGameObject;
	private Transform localTransform;

	private AICarController aiCarController;

	private BoxCollider boxCollider;
	private Rigidbody rigidbodyComponent;
	public float unitLenght;
	public bool slowed = false;

	void Awake() {
		localGameObject = this.gameObject;
		localTransform = this.transform;
		boxCollider = GetComponent<BoxCollider>();
		unitLenght = boxCollider.size.z;
		rigidbodyComponent = GetComponent<Rigidbody>();
		aiCarController = GetComponent<AICarController>();
	}

	void OnEnable() {
		slowed = false;
		StartCoroutine("ChangeTopSpeed");
		StartCoroutine("CheckOtherCars");
	}

	void OnBecameInvisible() {
		slowed = false;
		TrafficManager.instance.TakeTrafficUnit(this.gameObject);
		StopAllCoroutines();
	}

	void OnCollisionEnter(Collision collision) {
		if(collision.collider.tag == "CarAI") {
			TrafficManager.instance.TakeTrafficUnit(collision.collider.gameObject);
		}
	}

	IEnumerator ChangeTopSpeed() {
		while(true) {
			yield return new WaitForSeconds(5f);
			aiCarController.MaxSpeed = UnityEngine.Random.Range(8f, 20f);
			slowed = false;
		}
	}

	IEnumerator CheckOtherCars() {
		while(true) {
			yield return new WaitForSeconds(.5f);
			if(!slowed) {
				Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10f);

				for(int i = 0; i < hitColliders.Length; i++) {
					float angle = Vector3.Angle(transform.forward, hitColliders[i].transform.forward);
					Debug.Log(angle);
					float distance = Vector3.Distance(hitColliders[i].transform.position, transform.position);
					if(hitColliders[i] != boxCollider && hitColliders[i].tag == "CarAI" && distance < 10f && (angle >= 0f && angle <= 90f)) {
						if(hitColliders[i].GetComponent<TrafficUnit>().slowed == false) {
							hitColliders[i].GetComponent<AICarController>().MaxSpeed -= aiCarController.CurrentSpeed;
							hitColliders[i].GetComponent<TrafficUnit>().slowed = true;
							#if UNITY_EDITOR
							Debug.Log(name + " slowed: " + hitColliders[i].name);
							#endif
						}
					}
				}	
			}
		}
	}

	public void EnableUnit (Vector3 posotion, Quaternion rotation) {
		localTransform.position = posotion;
		localTransform.rotation = rotation;
		localGameObject.SetActive(true);
	}

	public void DisableUnit () {
		localGameObject.SetActive(false);
	}
}
