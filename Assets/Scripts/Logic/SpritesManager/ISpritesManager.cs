﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ISpritesManager : IService {

	List<Sprite> NormalSpritesList { get; }
	List<Sprite> HoverSpritesList { get; }
	List<Sprite> GameModeButtonSpritesList { get; }

	void LoadNormalSprites ();
	void LoadHoverSprites ();
	void LoadGameModeButtonSprites ();
}
