﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpritesManager : ISpritesManager {
	
	public List<Sprite> NormalSpritesList { get ; private set ; }
	public List<Sprite> HoverSpritesList  { get ; private set ; }
	public List<Sprite> GameModeButtonSpritesList { get ; private set ; }

	public void LoadNormalSprites () {
		NormalSpritesList = new List<Sprite>();
		NormalSpritesList.Clear();
	}

	public void LoadHoverSprites () {
		HoverSpritesList = new List<Sprite>();
		HoverSpritesList.Clear();
	}

	public void LoadGameModeButtonSprites () {
		GameModeButtonSpritesList = new List<Sprite>();
		GameModeButtonSpritesList.Clear();
		for(int i = 0; i < Resources.LoadAll("Sprites/MainMenu/Buttons").Length; i++) {
			if(Resources.LoadAll("Sprites/MainMenu/Buttons")[i].GetType() == typeof(Sprite)) {
				Debug.Log("El: " + Resources.LoadAll("Sprites/MainMenu/Buttons")[i]);
				GameModeButtonSpritesList.Add(Resources.LoadAll("Sprites/MainMenu/Buttons")[i] as Sprite);
			}
		}
	}

	public SpritesManager() {
		LoadNormalSprites();
		LoadHoverSprites();
		LoadGameModeButtonSprites();
	}
}
