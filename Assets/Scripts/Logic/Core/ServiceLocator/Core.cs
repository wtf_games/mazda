﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class Core : MonoBehaviour
{
	public static Core Instance;
    
	public ContextRoot contextRoot;

	private Dictionary<object, object> _services;

	void Awake() {
		//DontDestroyOnLoad(this.gameObject);

	}

	void Start() {
		if (Instance == null) {
			InitCore ();
		} else if (Instance != this) {
			Destroy(this.gameObject);
		}
	}

	void InitCore()
	{
		Instance = this;

		if(contextRoot == null) {
			contextRoot = GameObject.Find("Canvas").GetComponent<ContextRoot>();
		}

		_services = new Dictionary<object, object> ();
		//Init services (Managers)
		_services.Add(typeof(ISpritesManager), new SpritesManager());
		_services.Add (typeof(IGameManager), new GameManager());
		_services.Add(typeof(IStateMachine), new StatesManager());
		//Add UI contexts to states
	}


    private Action _sceneReadyCallback;
    public void LoadScene(Action SceneReadyCallback)
    {
        var gm = GetService<IGameManager>();
        if (gm != null)
        {
            _sceneReadyCallback = SceneReadyCallback;
			SceneManager.LoadSceneAsync(gm.CurrentGameType.ToString(), LoadSceneMode.Single);
        }
        
    }

    void OnLevelWasLoaded()
    {
        if (_sceneReadyCallback != null)
        {
            _sceneReadyCallback();
        }

		Debug.Log("Was loaded");

		if(contextRoot == null) {
			contextRoot = GameObject.Find("Canvas").GetComponent<ContextRoot>();
		}
    }

	public T GetService<T>()
	{
		
		return (T)_services [typeof(T)];
	}
}
