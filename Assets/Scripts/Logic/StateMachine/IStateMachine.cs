﻿using System.Collections.Generic;

public interface IStateMachine : IService
{
	IState CurrentState { get; }

	Dictionary<StatesEnum, IState> States { get; }

	void SwitchState (StatesEnum to_state);

	//Use UniRx for update in states !!!
}
