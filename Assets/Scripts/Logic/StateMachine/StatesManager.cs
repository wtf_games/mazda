﻿using UnityEngine;
using System.Collections.Generic;

public class StatesManager : IStateMachine
{
	public Dictionary<StatesEnum, IState> States { get ; private set; }

	public IState CurrentState { get ; private set; }
		
	public void SwitchState (StatesEnum to_state)
	{
		if(States.ContainsKey(to_state))
		{
			if (CurrentState != null) {
				CurrentState.Disable ();
			}

			CurrentState = States [to_state];
			CurrentState.Enable ();
		}
	}

	public StatesManager()
	{
		States = new Dictionary<StatesEnum, IState> ();

		//Init states
		States.Add(StatesEnum.Menu, new MenuState());
		States.Add(StatesEnum.PlayPanel, new PlayPanelState());
		//States.Add(StatesEnum.Game, new GameState());
		//States.Add(StatesEnum.Results, new ResultState());

		CurrentState = States [StatesEnum.Menu];
		CurrentState.Enable ();
	}

	
}
