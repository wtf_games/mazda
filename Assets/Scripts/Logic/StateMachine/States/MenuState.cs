﻿using UnityEngine;
using System.Collections;
using Slash.Unity.DataBind.Core.Data;

public class MenuState : IState
{
	private MenuContext _context;

	public void Enable ()
	{
		//Reset selected game
		Core.Instance.GetService<IGameManager> ().Reset ();

		_context.Show();
		_context.OnGameSelectedAction += SelectGame;
        _context.OnVideoSelectedAction += SelectVideo;
    }

	public void Disable ()
	{
		_context.Hide();
		_context.OnGameSelectedAction -= SelectGame;
        _context.OnVideoSelectedAction -= SelectVideo;
    }
		
	public MenuState()
	{
		//Init menu ui context
		_context = Core.Instance.contextRoot.GetCoontext<MenuContext> ();
		_context.Hide();
	}


	public void SelectGame(GameTypeEnum gameType)
	{
		Debug.Log ("GAME SELECTED !!! " + gameType);
        Core.Instance.GetService<IGameManager> ().SelectGame(gameType);
	}

    public void SelectVideo(GameTypeEnum gameType)
    {
        Debug.Log("VIDEO SELECTED !!! " + gameType);
        Core.Instance.GetService<IGameManager>().SelectVideo(gameType);
    }
}
