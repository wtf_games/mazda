﻿using UnityEngine;
using System.Collections;
using Slash.Unity.DataBind.Core.Data;

public class GameState : IState
{
	private GameContext _context;

	public void Enable ()
	{
		_context.Show();
		Core.Instance.GetService<IGameManager> ().GameHandler.InitGame ();
		_context.OnExitGameAction += OnExitGame;
	}

	public void Disable ()
	{
		_context.Hide();
		_context.OnExitGameAction -= OnExitGame;
	}


	public GameState()
	{
		_context = Core.Instance.contextRoot.GetCoontext<GameContext> ();
		_context.Hide();
	}

	void OnExitGame()
	{
		Core.Instance.GetService<IGameManager> ().GameHandler.ExitGame();
		Core.Instance.GetService<IStateMachine> ().SwitchState (StatesEnum.Menu);
	}

}