﻿using UnityEngine;
using System.Collections;

public class StartScreenState : IState
{
	private StartScreenContext _startScreenContext;

    public void Enable ()
	{
        _startScreenContext.Show();
        Debug.Log ("StartScreenState: " + Core.Instance.GetService<IGameManager> ().GameHandler);
	}

	public void Disable ()
	{
		_startScreenContext.Hide();
	}


	public StartScreenState()
	{
		_startScreenContext = Core.Instance.contextRoot.GetCoontext<StartScreenContext> ();
	}

	public void OnPlay()
	{
		Debug.Log("Click Play!");
		Core.Instance.GetService<IStateMachine> ().SwitchState (StatesEnum.Game);
	}
}