﻿using UnityEngine;
using System.Collections;

public class ResultState : IState
{
	private ResultsContext _context;

	public void Enable ()
	{
		_context.OnBackToMenuAction += BackToMenu;
		_context.Show();

	}
	public void Disable ()
	{
		_context.Hide();
		_context.OnBackToMenuAction -= BackToMenu;
	}


	public ResultState()
	{
		_context = Core.Instance.contextRoot.GetCoontext<ResultsContext> ();
		_context.Hide();
	}


	public void BackToMenu()
	{
		Core.Instance.GetService<IStateMachine> ().SwitchState (StatesEnum.Menu);
	}

}