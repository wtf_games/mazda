﻿using UnityEngine;
using System.Collections;

public class PlayPanelState : IState
{
    private PlayPanelContext _context;

    public void Enable()
    {
        Debug.Log("PlayPanelState: " + Core.Instance.GetService<IGameManager>().GameHandler);
        _context.Show();
    }

    public void Disable()
    {
        _context.Hide();
    }


    public PlayPanelState()
    {
        _context = Core.Instance.contextRoot.GetCoontext<PlayPanelContext>();
        _context.OnGameProgressBarDone += OnGameReadyToLoad;
    }

    public void OnGameReadyToLoad()
    {
        Core.Instance.LoadScene(OnGameReadyToPlay);
        
        //Core.Instance.GetService<IStateMachine>().SwitchState(StatesEnum.Game);
    }

    public void OnGameReadyToPlay()
    {
        Debug.Log("Scene Started...");
    }
}
