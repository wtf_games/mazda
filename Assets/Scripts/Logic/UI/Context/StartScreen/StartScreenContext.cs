﻿using UnityEngine;
using Slash.Unity.DataBind.Core.Data;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartScreenContext : Context, IWindowContext
{
    public Action OnGameLoaded;

	private readonly Property<string> descriptionTextProperty = new Property<string>();
	public string DescriptionText
	{
		get
		{
			return this.descriptionTextProperty.Value;
		}
		set
		{
			this.descriptionTextProperty.Value = value;
		}
	}

	private readonly Property<Sprite> startScreenSpriteProperty = new Property<Sprite>();

	public Sprite StartScreenSprite
	{
		get
		{
			return this.startScreenSpriteProperty.Value;
		}
		set
		{
			this.startScreenSpriteProperty.Value = value;
		}
	}

    #region IWindowContext implementation

    private readonly Property<float> startProgressProperty = new Property<float>();

    public float StartProgress
    {
        get
        {
            return this.startProgressProperty.Value;
        }
        private set
        {
            this.startProgressProperty.Value = value;
        }
    }

    private readonly Property<float> isVisibleProperty = new Property<float>();
	public bool IsVisible
	{
		get
		{
			return this.isVisibleProperty.Value == 1f;
		}
		private set
		{
			this.isVisibleProperty.Value = value ? 1f : 0f;
		}
	}

	public void Show ()
	{
		#if UNITY_EDITOR
		Debug.Log ("Show " + this.GetType());
		#endif

        StartProgress = 0f;

        DescriptionText = Core.Instance.GetService<IGameManager>().GameHandler.GameModeDescription;
        //StartScreenSprite = Core.Instance.GetService<IGameManager>().GameHandler.StartScreenPicture;

        IsVisible = true;
        Core.Instance.StartCoroutine(ProgressCatutine());
	}

    private IEnumerator ProgressCatutine()
    {
        float time = 0f;
        while (time < 5)
        {
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            StartProgress = time / 5.0f;
        }

        if (OnGameLoaded != null)
        {
            OnGameLoaded();
        }
    }

	public void Hide ()
	{
		Debug.Log ("Hide " + this.GetType());
		IsVisible = false;
	}
	#endregion

	//Set text and image for start screen, get from game manager -> IGameHandler

}