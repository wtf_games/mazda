﻿using UnityEngine;
using System.Collections.Generic;
using Slash.Unity.DataBind.Core.Presentation;

public class ContextRoot : MonoBehaviour
{
	public List<ContextHolder> _holders;

	public T GetCoontext<T>()
	{
		return (T)_holders.Find (q => q.ContextType == typeof(T)).Context;
	}
}
