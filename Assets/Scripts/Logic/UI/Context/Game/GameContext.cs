﻿using UnityEngine;
using Slash.Unity.DataBind.Core.Data;
using System;

public class GameContext : Context, IWindowContext
{
	public Action OnExitGameAction;

	#region IWindowContext implementation
	private readonly Property<float> isVisibleProperty = new Property<float>();

	public bool IsVisible
	{
		get
		{
			return this.isVisibleProperty.Value == 1f;
		}
		private set
		{
			this.isVisibleProperty.Value = value ? 1f : 0f;
		}
	}

	public void Show ()
	{
		Debug.Log ("Show " + this.GetType());
		IsVisible = true;
	}
	public void Hide ()
	{
		Debug.Log ("Hide " + this.GetType());
		IsVisible = false;
	}
	#endregion

	public GameContext()
	{

	}

	public void OnExitGame()
	{
		if (OnExitGameAction != null)
			OnExitGameAction ();
	}
}