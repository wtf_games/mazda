﻿using UnityEngine;
using Slash.Unity.DataBind.Core.Data;
using System;

public class ResultsContext : Context, IWindowContext
{
	public Action OnOpenVideoUrlAction;
	public Action OnOpenMazdaUrlAction;
	public Action OnBackToMenuAction;

	#region IWindowContext implementation
	private readonly Property<float> isVisibleProperty = new Property<float>();

	public bool IsVisible
	{
		get
		{
			return this.isVisibleProperty.Value == 1f;
		}
		private set
		{
			this.isVisibleProperty.Value = value ? 1f : 0f;
		}
	}

	public void Show ()
	{
		Debug.Log ("Show " + this.GetType());
		IsVisible = true;
	}
	public void Hide ()
	{
		Debug.Log ("Hide " + this.GetType());
		IsVisible = false;
	}
	#endregion

	public void OpenVideoUrl()
	{
		if (OnOpenVideoUrlAction != null)
			OnOpenVideoUrlAction ();	
	}

	public void OpenMazdaUrl()
	{
		if (OnOpenMazdaUrlAction != null)
			OnOpenMazdaUrlAction ();	
	}

	public void BackToMenu()
	{
		if (OnBackToMenuAction != null)
			OnBackToMenuAction ();	
	}
}