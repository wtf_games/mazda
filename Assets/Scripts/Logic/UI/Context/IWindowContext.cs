﻿public interface IWindowContext
{
	bool IsVisible{ get;}
	void Show();
	void Hide ();
}
