﻿using UnityEngine;
using System.Collections;
using Slash.Unity.DataBind.Core.Data;
using System;

public class PlayPanelContext : Context, IWindowContext
{
    public Action OnGameProgressBarDone;

    private readonly Property<float> isVisibleProperty = new Property<float>();
    public bool IsVisible
    {
        get
        {
            return this.isVisibleProperty.Value == 1f;
        }
        private set
        {
            this.isVisibleProperty.Value = value ? 1f : 0f;
        }
    }

    private readonly Property<string> gameModeProperty = new Property<string>();
    public string GameMode
    {
        get
        {
            return this.gameModeProperty.Value;
        }
        private set
        {
            this.gameModeProperty.Value = value;
        }
    }

    private StartScreenContext _startScreenContext;


    public void Hide()
    {
        IsVisible = false;
        _startScreenContext.Hide();
    }

    public void Show()
    {
        if (_startScreenContext == null)
        {
            _startScreenContext = Core.Instance.contextRoot.GetCoontext<StartScreenContext>();
            _startScreenContext.OnGameLoaded = () => 
            {
                if (OnGameProgressBarDone != null) OnGameProgressBarDone();
            };
        }

        IsVisible = true;
        GameMode = Core.Instance.GetService<IGameManager>().CurrentGameType.ToString();
        Debug.Log(GameMode);
       
        if (Core.Instance.GetService<IGameManager>().NeedToPlayVideo)
        {
            Debug.LogError("Play " + Core.Instance.GetService<IGameManager>().CurrentGameType + " video");
            _startScreenContext.Show();
        }
        else
        {
            _startScreenContext.Show();
        }
    }
}
