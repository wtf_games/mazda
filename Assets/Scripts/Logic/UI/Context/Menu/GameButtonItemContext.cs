﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Slash.Unity.DataBind.Core.Data;

public class GameButtonItemContext : Context
{

	public Action<GameTypeEnum> OnItemSelectedAction;
    public Action<GameTypeEnum> OnPlayVideoAction;

    private readonly GameTypeEnum _gameType;

	private readonly Property<string> gameTitleProperty = new Property<string>();
	public string GameTitle
	{
		get
		{
			return this.gameTitleProperty.Value;
		}
		set
		{
			this.gameTitleProperty.Value = value;
		}
	}

    private readonly Property<string> gameDescriptionProperty = new Property<string>();
    public string GameDescription
    {
        get
        {
            return this.gameDescriptionProperty.Value;
        }
        set
        {
            this.gameDescriptionProperty.Value = value;
        }
    }

	private readonly Property<bool> interactibleProperty = new Property<bool>();
	public bool Interactible
	{
		get
		{
			return this.interactibleProperty.Value;
		}
		set
		{
			this.interactibleProperty.Value = value;
		}
	}

	private readonly Property<Sprite> normalSpriteProperty = new Property<Sprite>();
	public Sprite NormalSprite
	{
		get
		{
			return this.normalSpriteProperty.Value;
		}
		set
		{
			this.normalSpriteProperty.Value = value;
		}
	}

	private readonly Property<Sprite> hoverSpriteProperty = new Property<Sprite>();
	public Sprite HoverSprite
	{
		get
		{
			return this.hoverSpriteProperty.Value;
		}
		set
		{
			this.hoverSpriteProperty.Value = value;
		}
	}

	private readonly Property<Sprite> buttonSpriteProperty = new Property<Sprite>();
	public Sprite ButtonSprite
	{
		get
		{
			return this.buttonSpriteProperty.Value;
		}
		set
		{
			this.buttonSpriteProperty.Value = value;
		}
	}

	public GameButtonItemContext(
		GameTypeEnum gameType,
		string gameTitle,
		string gameDescription,
		bool interactible,
		Sprite normalSprite,
		Sprite hoverSprite,
		Sprite buttonSprite
	)
	{
		_gameType = gameType;
		GameTitle = gameTitle;
        GameDescription = gameDescription;
		Interactible = interactible;
		NormalSprite = normalSprite;
		HoverSprite = hoverSprite;
		ButtonSprite = buttonSprite;
	}

	private Sprite originalSprite;

	public void OnItemSelected()
	{
        Debug.Log("OnItemSelected: " + _gameType);
		if (OnItemSelectedAction != null)
        {
			OnItemSelectedAction (_gameType);
        }
	}

	public void OnItemPointerEnter() {
		Debug.Log("OnItemPointerEnter");
		originalSprite = NormalSprite;
		NormalSprite = HoverSprite;
	}

	public void OnItemPointerExit() {
		Debug.Log("OnItemPointerExit");
		NormalSprite = originalSprite;
	}

    public void OnPlayVideo()
    {
        Debug.Log("OnPlayVideo: " + _gameType);
        if (OnPlayVideoAction != null)
        {
            OnPlayVideoAction(_gameType);
        }
    }
}
