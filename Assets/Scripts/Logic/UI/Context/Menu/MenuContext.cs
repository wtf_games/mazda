﻿using UnityEngine;
using UnityEngine.UI;
using Slash.Unity.DataBind.Core.Data;
using System.Collections.Generic;
using System;


public class MenuContext : Context, IWindowContext
{

	public Action<GameTypeEnum> OnGameSelectedAction;
    public Action<GameTypeEnum> OnVideoSelectedAction;

    private readonly Collection<GameButtonItemContext> _items = new Collection<GameButtonItemContext> ();
	public Collection<GameButtonItemContext> Items
	{
		get { return this._items; }
	}

	private readonly Property<float> isVisibleProperty = new Property<float>();

	public bool IsVisible
	{
		get
		{
			return this.isVisibleProperty.Value == 1f;
		}
		private set
		{
			this.isVisibleProperty.Value = value ? 1f : 0f;
		}
	}

	public List<Sprite> normalSpritesList { get ; private set ; }
	public List<Sprite> hoverSpritesList  { get ; private set ; }
	public List<Sprite> gameModeButtonSpritesList { get ; private set ; }

	public MenuContext()
	{
		LoadNormalSprites();
		LoadHoverSprites();
		LoadGameModeButtonSprites();

		Items.Add (new GameButtonItemContext (
			GameTypeEnum.ALH,
			"НОЧНАЯ ПОЕЗДКА",
			"АДАПТИВНЫЕ СВЕТОДИОДНЫЕ ФАРЫ (ALH)",
			true,
			normalSpritesList[(int)GameTypeEnum.ALH],
			hoverSpritesList[(int)GameTypeEnum.ALH],
			gameModeButtonSpritesList[(int)GameTypeEnum.ALH]
		));

		Items.Add (new GameButtonItemContext (
			GameTypeEnum.LRC,
			"ДОЛГАЯ ДОРОГА",
			"СИСТЕМПА ПРЕДУПРЕЖДЕНИЯ О ВЫХОДЕ С ЗАНИМАЕМОЙ ПОЛОСЫ(LAS)",
			true,
			normalSpritesList[(int)GameTypeEnum.LRC],
			hoverSpritesList[(int)GameTypeEnum.LRC],
			gameModeButtonSpritesList[(int)GameTypeEnum.LRC]
		));

		Items.Add (new GameButtonItemContext (
			GameTypeEnum.CTJ,
			"ГОРОДСКАЯ ПРОБКА",
			"СИСТЕМА БЕЗОПАСНОГО ТОРМОЖЕНИЯ В ГОРОДЕ (SCBS)",
			false,
			normalSpritesList[(int)GameTypeEnum.CTJ],
			hoverSpritesList[(int)GameTypeEnum.CTJ],
			gameModeButtonSpritesList[(int)GameTypeEnum.CTJ]
		));

		Items.Add (new GameButtonItemContext (
			GameTypeEnum.HDT,
			"ПЛОТНЫЙ ТРАФИК",
			"СИСТЕМА МОНИТОРИНГА МЕРТВЫХ ЗОН (BSM)",
			false,
			normalSpritesList[(int)GameTypeEnum.HDT],
			hoverSpritesList[(int)GameTypeEnum.HDT],
			gameModeButtonSpritesList[(int)GameTypeEnum.HDT]
		));
	}

	private void SelectGameMode(GameTypeEnum selectedGameMode)
	{
		if (OnGameSelectedAction != null) {
			OnGameSelectedAction (selectedGameMode);
		}
	}

    private void SelectVideo(GameTypeEnum selectedGameMode)
    {
        if (OnVideoSelectedAction != null)
        {
            OnVideoSelectedAction(selectedGameMode);
        }
    }

	private void LoadNormalSprites () {
		normalSpritesList = new List<Sprite>();
		normalSpritesList.Clear();
		object[] objects = Resources.LoadAll("Sprites/MainMenu/Backgrounds/Normal");
		for(int i = 0; i < objects.Length; i++) {
			if(objects[i].GetType() == typeof(Sprite)) {
				#if UNITY_EDITOR
				//Debug.Log("El: " + objects[i]);
				#endif
				normalSpritesList.Add(objects[i] as Sprite);
			}
		}
	}

	private void LoadHoverSprites () {
		hoverSpritesList = new List<Sprite>();
		hoverSpritesList.Clear();
		object[] objects = Resources.LoadAll("Sprites/MainMenu/Backgrounds/Hover");
		for(int i = 0; i < objects.Length; i++) {
			if(objects[i].GetType() == typeof(Sprite)) {
				#if UNITY_EDITOR
				//Debug.Log("El: " + objects[i]);
				#endif
				hoverSpritesList.Add(objects[i] as Sprite);
			}
		}
	}

	private void LoadGameModeButtonSprites () {
		gameModeButtonSpritesList = new List<Sprite>();
		gameModeButtonSpritesList.Clear();
		object[] objects = Resources.LoadAll("Sprites/MainMenu/Buttons");
		for(int i = 0; i < objects.Length; i++) {
			if(objects[i].GetType() == typeof(Sprite)) {
				#if UNITY_EDITOR
				Debug.Log("El: " + objects[i]);
				#endif
				gameModeButtonSpritesList.Add(objects[i] as Sprite);
			}
		}
	}

    #region IWindowContext implementation
    public void Show ()
	{
		Debug.Log ("Show " + this.GetType());

		foreach (var btnContext in Items) {
			((GameButtonItemContext)btnContext).OnItemSelectedAction += SelectGameMode;
            ((GameButtonItemContext)btnContext).OnItemSelectedAction += SelectVideo;

        }

		IsVisible = true;
	}
	public void Hide ()
	{
		Debug.Log ("Hide " + this.GetType());

		foreach (var btnContext in Items) {
			((GameButtonItemContext)btnContext).OnItemSelectedAction -= SelectGameMode;
            ((GameButtonItemContext)btnContext).OnItemSelectedAction -= SelectVideo;
        }

		IsVisible = false;
	}
	#endregion
}