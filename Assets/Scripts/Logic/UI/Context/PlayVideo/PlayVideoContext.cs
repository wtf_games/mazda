﻿using UnityEngine;
using Slash.Unity.DataBind.Core.Data;
using System;

public class PlayVideoContext : Context, IWindowContext
{
    private readonly Property<Sprite> startScreenSpriteProperty = new Property<Sprite>();

    public Sprite StartScreenSprite
    {
        get
        {
            return this.startScreenSpriteProperty.Value;
        }
        set
        {
            this.startScreenSpriteProperty.Value = value;
        }
    }

    #region IWindowContext implementation

    private readonly Property<float> isVisibleProperty = new Property<float>();
    public bool IsVisible
    {
        get
        {
            return this.isVisibleProperty.Value == 1f;
        }
        private set
        {
            this.isVisibleProperty.Value = value ? 1f : 0f;
        }
    }

    public void Show()
    {
        Debug.Log("Show " + this.GetType());
   
        //StartScreenSprite = Core.Instance.GetService<IGameManager>().GameHandler.StartScreenPicture;

        IsVisible = true;
    }

    public void Hide()
    {
        Debug.Log("Hide " + this.GetType());
        IsVisible = false;
    }
    #endregion

    //Set text and image for start screen, get from game manager -> IGameHandler

}
