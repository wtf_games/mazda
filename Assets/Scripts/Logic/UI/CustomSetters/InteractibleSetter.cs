﻿using UnityEngine;
using UnityEngine.UI;
using Slash.Unity.DataBind.Foundation.Setters;

[AddComponentMenu("Data Bind/UnityUI/Setters/[DB] Interactible Setter (Unity)")]
public class InteractibleSetter : ComponentSingleSetter<Button, bool>
{
	#region Methods

	protected override void OnValueChanged(bool newValue)
	{
		this.Target.interactable = newValue;
	}

	#endregion
}
