﻿using UnityEngine;
using UnityEngine.UI;
using Slash.Unity.DataBind.Foundation.Setters;

[AddComponentMenu("Data Bind/UnityUI/Setters/[DB] Visibility Setter (Unity)")]
public class VisibilitySetter : ComponentSingleSetter<CanvasGroup, float>
{
	#region Methods

	protected override void OnValueChanged(float newValue)
	{
		this.Target.alpha = newValue;
		this.Target.blocksRaycasts = (newValue == 1f);
	}

	#endregion
}
