﻿using UnityEngine;
using UnityEngine.UI;
using Slash.Unity.DataBind.Foundation.Setters;

[AddComponentMenu("Data Bind/UnityUI/Setters/[DB]String Visibility Setter (Unity)")]
public class StringVisibilitySetter : ComponentSingleSetter<CanvasGroup, string>
{
    public string targetValue;

    #region Methods

    protected override void OnValueChanged(string newValue)
    {
        this.Target.alpha = (newValue == targetValue) ? 1: 0;
        this.Target.blocksRaycasts = (newValue == targetValue);
    }

    #endregion
}
