﻿namespace Slash.Unity.DataBind.UI.Unity.Commands
{
	using Slash.Unity.DataBind.Foundation.Commands;

	using UnityEngine;
	using UnityEngine.EventSystems;

	/// <summary>
	///   Command which is invoked when the pointner over element.
	///   Parameters:
	///   - Pointer event data.
	/// </summary>
	[AddComponentMenu("Data Bind/UnityUI/Commands/[DB] Pointer Enter Command (Unity)")]
	public class PointerEnterCommand : Command, IPointerEnterHandler
	{
		#region Public Methods and Operators

		public void OnPointerEnter(PointerEventData eventData) {
			this.InvokeCommand(eventData);
		}

		#endregion
	}
}