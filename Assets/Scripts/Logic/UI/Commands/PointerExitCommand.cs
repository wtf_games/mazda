﻿namespace Slash.Unity.DataBind.UI.Unity.Commands
{
	using Slash.Unity.DataBind.Foundation.Commands;

	using UnityEngine;
	using UnityEngine.EventSystems;

	/// <summary>
	///   Command which is invoked when the pointner over element.
	///   Parameters:
	///   - Pointer event data.
	/// </summary>
	[AddComponentMenu("Data Bind/UnityUI/Commands/[DB] Pointer Exit Command (Unity)")]
	public class PointerExitCommand : Command, IPointerExitHandler
	{
		#region Public Methods and Operators

		public void OnPointerExit(PointerEventData eventData) {
			this.InvokeCommand(eventData);
		}

		#endregion
	}
}