﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LRCGameHandler : IGameModeHandler
{
    #region IGameModeHandler implementation

    public event Action<IGameResult> OnGameFinished;

    public Sprite StartScreenPicture
    {
        get;
        private set;
    }
    public string GameModeDescription
    {
        get;
        private set;
    }
    public RoadManager RoadManager
    {
        get;
        private set;
    }
    public WayPoints WP
    {
        get;
        private set;
    }
    public List<IGameSituation> Situations
    {
        get;
        private set;
    }

    public void LoadGameModeData()
    {
        //Load or hardcode game mode data
        GameModeDescription = "Управляйте автомобилем, двигайтесь в заданной полосе, и не пересекайте линии разметки.";
        //StartScreenPicture = Resources.Load ("UI/ALH/StartScreenSprite") as Sprite;


        //TODO sutuations
    }
    public void InitGame()
    {
        RoadManager.CreateRoad(Core.Instance.GetService<IGameManager>().CurrentGameType);
        //Set first situation, start game timer
    }

    public void ExitGame()
    {
        RoadManager.DestroyRoad();
    }

    public void StartNextSituation()
    {
        //
    }


    public void SwitchToDetalized()
    {
        //Set car and road prefabs to detalized
    }
    public void CalculateGameResult()
    {
        //
    }
    public void StartFreeRace()
    {
        //Create road, set traffic and input parameters
    }
    public void FinishGame()
    {
        //Switch state to result screen
    }
    public void Destroy()
    {
        Debug.Log("Destroy Game handler");
    }

    #endregion
}