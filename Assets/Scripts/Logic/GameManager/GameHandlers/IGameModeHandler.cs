﻿using UnityEngine;
using System;
using System.Collections.Generic;

//use factory
public interface IGameModeHandler
{
	event Action<IGameResult> OnGameFinished;

	void LoadGameModeData();

	//Start screen
	Sprite StartScreenPicture { get; }//LoadGameModeData
	string GameModeDescription { get; }//LoadGameModeData

	//Game
	RoadManager RoadManager { get; }//LoadGameModeData
	List<IGameSituation> Situations { get; }//LoadGameModeData
	//float result - game summary result

	void InitGame ();
	//TrafficHandler -> instantiate road, init AI

	void ExitGame ();

	//add situation timer
	void StartNextSituation();

	//by timer or car-crash
	void SwitchToDetalized();

	void CalculateGameResult();

	//set infinite timer
	void StartFreeRace();

	//By timer or user exit
	void FinishGame ();

	void Destroy ();
}
