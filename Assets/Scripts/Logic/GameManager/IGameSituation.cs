﻿using System;

public interface IGameSituation
{
	event Action<bool, float> OnSituationComplete;

	void Start();
	void End();
	void Break();
}
