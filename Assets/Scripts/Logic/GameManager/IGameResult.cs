﻿public interface IGameResult
{
	string message { get; }
}
