﻿using UnityEngine;

public interface IGameManager
{
	GameTypeEnum CurrentGameType { get; }

	IGameModeHandler GameHandler { get; }

    bool NeedToPlayVideo { get; }

    void SelectVideo(GameTypeEnum gameType);

    //Set current game type
    void SelectGame (GameTypeEnum gameType);

	//Destroy GameHandler
	void Reset();

}
