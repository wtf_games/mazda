﻿using UnityEngine;
using System.Collections;

public class GameManager : IGameManager
{
	public GameTypeEnum CurrentGameType
    {
		get;
		private set;
	}

    public bool NeedToPlayVideo
    {
        get;
        private set;
    }

	public IGameModeHandler GameHandler
    {
		get;
		private set;
	}

    public void SelectVideo(GameTypeEnum gameType)
    {
        NeedToPlayVideo = true;
        CreateGameHandler(gameType);
    }

	public void SelectGame (GameTypeEnum gameType)
	{
        NeedToPlayVideo = false;
        CreateGameHandler(gameType);
    }

    private void CreateGameHandler(GameTypeEnum gameType)
    {
        CurrentGameType = gameType;

        switch (gameType)
        {
            case GameTypeEnum.LRC:
                GameHandler = new LRCGameHandler();
                break;
            case GameTypeEnum.ALH:
                GameHandler = new ALHGameHandler();
                break;

            case GameTypeEnum.CTJ:

                break;
            case GameTypeEnum.HDT:

                break;


            default:
                break;
        }

        if (GameHandler != null)
        {
            GameHandler.LoadGameModeData();
            Core.Instance.GetService<IStateMachine>().SwitchState(StatesEnum.PlayPanel);
        }
    }

	public void Reset ()
	{
		if (GameHandler != null)
		{
			GameHandler.Destroy ();
		}
	}

	
}
